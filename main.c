#include <avr/io.h>
#include <util/delay.h>
#include "main.h"
#include "sreg.h"

void main(void) {
  sreg myRegister = {&PORTB, 0x01, 0x02, 0x04};
  Psreg myReg = &myRegister;

  DDRB |= (myReg->in | myReg->out_en | myReg->clk);
  sreg_init(myReg);
  while(1) {
    for(int i = 0x00; i < 0xFF; i++){
      sreg_push(i, myReg);
      _delay_ms(50);
    }
    for(int i = 0xFF; i > 0x00; i--){
      sreg_push(i, myReg);
      _delay_ms(50);
    }
  }
}

