#include <util/delay.h>
#include "sreg.h"

void sreg_init(Psreg sr) {
  *(sr->port) &= ~(sr->out_en); //enable out by default
  sreg_push(0x00, sr);
}

void sreg_push(char val, Psreg sr) {
  *(sr->port) |= sr->out_en;
  //SHIFT_PORT |= output_enable;
  for (int i = 0; i < 8; i++) {
    if(val & ( 1 << i)){
      *(sr->port) |= sr->in;
    } else {
      *(sr->port) &= ~(sr->in);
    }
    sreg_adv_clk(sr);
  }
  // advance clock - needed since we're always one behind
  sreg_adv_clk(sr);
  *(sr->port) &= ~(sr->out_en);
}

void sreg_adv_clk(Psreg sr) {
  // advance clock
  *(sr->port) |= sr->clk;
  *(sr->port) &= ~(sr->clk);
}
