/**
 * This code is mostly borrowed from here:
 * https://gist.github.com/adnbr/2439125
 * Used to count the number of milliseconds that have passed since boot
*/
#include<avr/interrupt.h>
#include<util/atomic.h>

#define CTC_MATCH_OVERFLOW ((F_CPU / 1000) /8)

typedef enum {MILLIS, SECS, MINS} period;


volatile unsigned long timer1 = 0;
volatile unsigned long seconds = 0;
volatile unsigned long minutes = 0;
/**
 * On fire of timer1 interrupt, increment millis
*/
ISR(TIMER1_COMPA_vect){
  timer1++;
  seconds = timer1/1000;
  minutes = seconds/60;
}
/**
 * Return length of period p, since boot
*/
unsigned long time(period p) {
  switch (p) {
    case MILLIS:
      return timer1;
    case SECS:
      return seconds;
    case MINS:
      return minutes;
    default:
      return 0;
  }
}
/**
 * Set up registers for clock to function, and enable interrupts
*/
void timer_init() {
  TCCR1B |= (1 << WGM12) | (1 << CS11);

  OCR1AH = (CTC_MATCH_OVERFLOW >> 8);
  OCR1AL = (CTC_MATCH_OVERFLOW);

  TIMSK1 |= (1 << OCIE1A);

  sei();
}
